import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue")
  },
  {
    path: "/analytics",
    name: "Analytics",
    component: () => import("../views/Home.vue")
  },
  {
    path: "/buscar",
    name: "Search",
    component: () => import("../views/Search.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/registro",
    name: "Logup",
    component: () => import("../views/Home.vue")
  },
  {
    path: "/nuevo",
    name: "NewProduct",
    component: () => import("../views/NewProduct.vue")
  },
  /* ----------- BACKOFFICE ----------- */
  {
    path: "/backoffice",
    name: "Dashboard",
    component: () => import("../views/backoffice/Dashboard.vue"),
    children: [
      {
        path: "/",
        name: "DashboardHome",
        component: () => import("../views/backoffice/DashboardHome.vue")
      },
      {
        path: "ciudad/:country/:statement/nueva",
        name: "NewCity",
        component: () => import("../views/backoffice/NewCity.vue")
      },
      {
        path: "ciudad/:country/:statement/:city",
        name: "City",
        component: () => import("../views/backoffice/City.vue")
      },
      {
        path: "ciudad/:country/:statement/:city/editar",
        name: "EditCity",
        component: () => import("../views/backoffice/EditCity.vue")
      },
      {
        path: "paises",
        name: "Countries",
        component: () => import("../views/backoffice/Countries.vue")
      },
      {
        path: "pais/nuevo",
        name: "NewCountry",
        component: () => import("../views/backoffice/NewCountry.vue")
      },
      {
        path: "pais/:country",
        name: "Country",
        component: () => import("../views/backoffice/Country.vue")
      },
      {
        path: "pais/:country/editar",
        name: "EditCountry",
        component: () => import("../views/backoffice/EditCountry.vue")
      },
      {
        path: "pais/:country/estados",
        name: "Statements",
        component: () => import("../views/backoffice/Statements.vue")
      },
      {
        path: "estado/:country/nuevo",
        name: "NewStatement",
        component: () => import("../views/backoffice/NewStatement.vue")
      },
      {
        path: "estado/:country/:statement",
        name: "Statement",
        component: () => import("../views/backoffice/Statement.vue")
      },
      {
        path: "estado/:country/:statement/editar",
        name: "EditStatement",
        component: () => import("../views/backoffice/EditStatement.vue")
      },
      {
        path: "estado/:country/:statement/ciudades",
        name: "Cities",
        component: () => import("../views/backoffice/Cities.vue")
      },
      {
        path: "usuarios",
        name: "Users",
        component: () => import("../views/backoffice/Users.vue")
      },
      {
        path: "usuario/:id",
        name: "User",
        component: () => import("../views/backoffice/User.vue")
      },
      {
        path: "*",
        name: "Error",
        component: () => import("../views/backoffice/Dashboard.vue")
      }
    ]
  },
  /* -----X----- BACKOFFICE -----X----- */
  {
    path: "/producto/:barcode",
    name: "Product",
    component: () => import("../views/Product.vue")
  },
  {
    path: "*",
    name: "Error",
    component: () => import("../views/Error.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;

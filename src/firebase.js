import firebase from "firebase/app";
require("firebase/auth");
require("firebase/firestore");

const firebaseConfig = {
  apiKey: "AIzaSyAE5b4EWDwIbFWBhxtf0Dno0bQPSGVOgz4",
  authDomain: "ciudadbusca-backend.firebaseapp.com",
  databaseURL: "https://ciudadbusca-backend.firebaseio.com",
  projectId: "ciudadbusca-backend",
  storageBucket: "ciudadbusca-backend.appspot.com",
  messagingSenderId: "888421494313",
  appId: "1:888421494313:web:c8ef2df77af588a7c4f638",
  measurementId: "G-2RZCYZW5CW"
};

export const fb = firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
export const fs = firebase.firestore();

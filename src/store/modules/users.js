import { fb, fs } from "@/firebase";

const db = fs.collection("users");

const state = {
  auth: "",
  user: "",
  users: []
};

const getters = {
  getUser: state => state.user,
  getUsers: state => state.users
};

const actions = {
  async login({ commit }, data) {
    fb.auth()
      .signInWithEmailAndPassword(data.email, data.password)
      .then(async response => {
        await commit("Auth", response);
      })
      .catch(error => {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == "auth/wrong-password") {
          alert("Wrong password");
        } else {
          alert(errorMessage);
        }
        console.log(error);
      });
  },
  register() {
    fb.auth()
      .createUserWithEmailAndPassword(this.email, this.password)
      .then(user => {
        this.addUser(user.user);

        this.reset();
        // eslint-disable-next-line no-undef
        $("#loginModal").modal("hide");

        this.$router.replace("admin/dashboard");
      })
      .catch(error => {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == "auth/weak-password") {
          alert("The password is too weak");
        } else {
          alert(errorMessage);
        }
        console.log(error);
      });
  },
  async addUser({ commit }, data) {
    const info = {
      displayName: data.displayName,
      email: data.email,
      emailVerified: data.emailVerified,
      phoneNumber: data.phoneNumber,
      photoURL: data.photoURL,
      providerId: data.providerId,
      uid: data.uid
    };

    await db
      .doc(data.uid)
      .set(info)
      .then(async response => {
        await commit("newUser", response);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchUser({ commit }, id) {
    await db
      .doc(id)
      .get()
      .then(doc => {
        console.log(doc.data());
        const user = {
          key: doc.id,
          state: doc.data().state,
          isActive: doc.data().isActive,
          name: doc.data().name
        };

        commit("setUser", user);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchUsers({ commit }) {
    await db
      .get()
      .then(async query => {
        let data = [];

        await query.forEach(item => {
          data.push(item.data());
        });

        await commit("setUsers", data);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  }
};

const mutations = {
  Auth: (state, user) => (state.auth = user),
  newUser: (state, user) => state.users.unshift(user),
  setUser: (state, user) => (state.user = user),
  setUsers: (state, users) => (state.users = users)
};

export default {
  state,
  getters,
  actions,
  mutations
};

import firebase from "firebase";
import { fs } from "@/firebase";

const db = fs.collection("products");

const state = {
  product: "",
  productPhotos: [],
  products: []
};

const getters = {
  getProduct: state => state.product,
  getProductPhotos: state => state.productPhotos,
  getProducts: state => state.products
};

const actions = {
  async addProduct({ commit }, data) {
    const info = {
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      description: data.description,
      legals: data.legals,
      slug: data.slug,
      title: data.title,
      isActive: true,
      isLock: false,
      uid: data.uid
    };

    await db
      .doc(data.barcode)
      .set(info)
      .then(async response => {
        await commit("newProduct", response);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchProduct({ commit }, id) {
    await db
      .doc(id)
      .get()
      .then(doc => {
        const product = {
          id: doc.id,
          createdAt: new Date(doc.data().createdAt?.toDate()).toUTCString(),
          description: doc.data().description,
          legals: doc.data().legals,
          photos: doc.data().photos,
          slug: doc.data().slug,
          title: doc.data().title,
          isActive: doc.data().isActive,
          isLock: doc.data().isLock,
          uid: doc.data().uid
        };

        commit("setProduct", product);
      });
  },
  async fetchProductPhotos({ commit }, id) {
    await fs
      .collection(`/products/${id}/photos`)
      .get()
      .then(async query => {
        let data = [];

        await query.forEach(async item => {
          const images = {
            id: item.id,
            image: item.data().image,
            order: item.data().order
          };

          data.push(images);
        });

        await commit("setProductPhotos", data);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchProducts({ commit }) {
    await db
      .where("uid", "==", "AlFLDDdsm7MhbBRJv2mvObczRAp1")
      .where("isActive", "==", true)
      .where("isLock", "==", false)
      .get()
      .then(async query => {
        const total = query.size;
        let data = [];

        await query.forEach(item => {
          const product = {
            id: item.id,
            createdAt: new Date(item.data().createdAt?.toDate()).toUTCString(),
            title: item.data().title,
            isActive: item.data().isActive,
            isLock: item.data().isLock
          };

          data.push(product);
        });

        await commit("setProducts", data, total);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  }
};

const mutations = {
  newProduct: (state, product) => state.products.unshift(product),
  setProduct: (state, product) => (state.product = product),
  setProductPhotos: (state, photos) => (state.productPhotos = photos),
  setProducts: (state, products) => (state.products = products)
};

export default {
  state,
  getters,
  actions,
  mutations
};

import { fs } from "@/firebase";

const state = {
  statement: "",
  statements: []
};

const getters = {
  getStatement: state => state.statement,
  getStatements: state => state.statements
};

const actions = {
  async addStatement({ commit }, data) {
    const info = {
      country: data.country,
      isActive: data.isActive,
      name: data.name
    };

    await fs
      .collection(`countries/${data.country}/states`)
      .doc(data.slug)
      .set(info)
      .then(async response => {
        await commit("newStatement", response);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchStatement({ commit }, data) {
    await fs
      .collection(`countries/${data.country}/states`)
      .doc(data.statement)
      .get()
      .then(doc => {
        const info = {
          key: doc.id,
          country: doc.data().country,
          isActive: doc.data().isActive,
          name: doc.data().name
        };

        commit("setStatement", info);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchStatements({ commit }, country) {
    await fs
      .collection(`countries/${country}/states`)
      .orderBy("name")
      .get()
      .then(async query => {
        let data = [];

        await query.forEach(doc => {
          const info = {
            key: doc.id,
            country: doc.data().country,
            isActive: doc.data().isActive,
            name: doc.data().name
          };

          data.push(info);
        });

        await commit("setStatements", data);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async updateStatement({ commit }, data) {
    const info = {
      country: data.country,
      isActive: data.isActive,
      name: data.name
    };

    await fs
      .collection(`countries/${data.country}/states`)
      .doc(data.slug)
      .set(info)
      .then(async response => {
        await commit("editStatement", response);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async removeStatement({ commit }, data) {
    const response = await fs
      .collection(`countries/${data.country}/states`)
      .doc(data.slug)
      .delete();

    commit("deleteStatement", response);
  }
};

const mutations = {
  deleteStatement: (state, statement) =>
    state.statements.splice(state.statements.indexOf(statement), 1),
  editStatement: (state, statement) => (state.statement = statement),
  newStatement: (state, statement) => state.statements.unshift(statement),
  setStatement: (state, statement) => (state.statement = statement),
  setStatements: (state, statements) => (state.statements = statements)
};

export default {
  state,
  getters,
  actions,
  mutations
};

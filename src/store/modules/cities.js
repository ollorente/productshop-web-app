import { fs } from "@/firebase";

const state = {
  city: "",
  cities: []
};

const getters = {
  getCity: state => state.city,
  getCities: state => state.cities
};

const actions = {
  async addCity({ commit }, data) {
    const info = {
      state: data.statement,
      isActive: data.isActive,
      name: data.name
    };

    await fs
      .collection(`countries/${data.country}/states/${data.statement}/cities`)
      .doc(data.slug)
      .set(info)
      .then(async response => {
        await commit("newStatement", response);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchCity({ commit }, data) {
    await fs
      .collection(`countries/${data.country}/states/${data.statement}/cities`)
      .doc(data.city)
      .get()
      .then(doc => {
        const info = {
          key: doc.id,
          state: doc.data().state,
          isActive: doc.data().isActive,
          name: doc.data().name
        };

        commit("setCity", info);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchCities({ commit }, data) {
    await fs
      .collection(`countries/${data.country}/states/${data.statement}/cities`)
      .orderBy("name")
      .get()
      .then(async query => {
        let data = [];

        await query.forEach(doc => {
          const info = {
            key: doc.id,
            state: doc.data().state,
            isActive: doc.data().isActive,
            name: doc.data().name
          };

          data.push(info);
        });

        await commit("setCities", data);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async updateCity({ commit }, data) {
    const info = {
      state: data.statement,
      isActive: data.isActive,
      name: data.name
    };
    console.log(info);
    await fs
      .collection(`countries/${data.country}/states/${data.statement}/cities`)
      .doc(data.slug)
      .set(info)
      .then(async response => {
        await commit("editCity", response);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async removeCity({ commit }, data) {
    const response = await fs
      .collection(`countries/${data.country}/states/${data.statement}/cities`)
      .doc(data.slug)
      .delete();

    commit("deleteCity", response);
  }
};

const mutations = {
  deleteCity: (state, city) =>
    state.cities.splice(state.cities.indexOf(city), 1),
  editCity: (state, city) => (state.city = city),
  newCity: (state, city) => state.cities.unshift(city),
  setCity: (state, city) => (state.city = city),
  setCities: (state, cities) => (state.cities = cities)
};

export default {
  state,
  getters,
  actions,
  mutations
};

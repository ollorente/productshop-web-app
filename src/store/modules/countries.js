import { fs } from "@/firebase";

const db = fs.collection("countries");

const state = {
  country: "",
  countries: []
};

const getters = {
  getCountry: state => state.country,
  getCountries: state => state.countries
};

const actions = {
  async addCountry({ commit }, data) {
    const info = {
      code: data.code ? data.code.toLowerCase() : "",
      currency: data.currency ? data.currency.toUpperCase() : "",
      isActive: data.isActive,
      name: data.name
    };

    await db
      .doc(data.slug)
      .set(info)
      .then(async response => {
        await commit("newCountry", response);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchCountry({ commit }, id) {
    await db
      .doc(id)
      .get()
      .then(async doc => {
        const data = {
          key: await doc.id,
          code: await doc.data().code,
          currency: await doc.data().currency,
          isActive: await doc.data().isActive,
          name: await doc.data().name
        };

        commit("setCountry", data);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async fetchCountries({ commit }) {
    await db
      .orderBy("name")
      .get()
      .then(async query => {
        const total = query.size;
        let data = [];

        await query.forEach(doc => {
          const info = {
            key: doc.id,
            code: doc.data().code,
            isActive: doc.data().isActive,
            name: doc.data().name
          };

          data.push(info);
        });

        await commit("setCountries", data, total);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async updateCountry({ commit }, data) {
    const info = {
      code: data.code,
      currency: data.currency,
      isActive: data.isActive,
      name: data.name
    };

    await db
      .doc(data.slug)
      .set(info)
      .then(async response => {
        await commit("editCountry", response);
      })
      .catch(function(error) {
        console.log("Error getting document:", error);
      });
  },
  async removeCountry({ commit }, data) {
    const response = await db.doc(data).delete();

    commit("deleteCountry", response);
  }
};

const mutations = {
  deleteCountry: (state, country) =>
    state.countries.splice(state.countries.indexOf(country), 1),
  editCountry: (state, country) => (state.country = country),
  newCountry: (state, country) => state.countries.unshift(country),
  setCountry: (state, country) => (state.country = country),
  setCountries: (state, countries) => (state.countries = countries)
};

export default {
  state,
  getters,
  actions,
  mutations
};

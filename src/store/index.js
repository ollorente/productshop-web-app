import Vue from "vue";
import Vuex from "vuex";
import city from "./modules/cities";
import country from "./modules/countries";
import product from "./modules/products";
import statement from "./modules/statements";
import user from "./modules/users";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    total: 0,
    page: 1,
    pages: 0,
    perPage: 10
  },
  mutations: {},
  actions: {},
  modules: {
    city,
    country,
    product,
    statement,
    user
  }
});
